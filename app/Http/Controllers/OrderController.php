<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function callback(Request $request)
    {
        $data = $request->all();
        $order = app(Order::class);
        $order->status = $data["transaction"]["status"];
        $order->data = json_encode($data);
        $order->save();
        $data["model"] = $order;

        return response()->json($data);
    }
}
